# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/09/02 00:56:20 by tfriedri          #+#    #+#              #
#    Updated: 2022/09/29 10:55:37 by tfriedri         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = philo
CC = cc
CFLAGS = -Wall -Werror -Wextra

SRCS =	./srcs/main.c			\
		./srcs/philo_thread.c		\
		./srcs/initialisation.c	\
		./srcs/utilities.c		\
		./srcs/free_and_error.c \
		./srcs/dead_check.c		\

OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJS)
	@$(CC) $(OBJS) -o $(NAME)

linux: $(OBJS)
	@$(CC) $(OBJS) -lpthread -o $(NAME) 

debug: fclean $(OBJS) #Linux
	@$(CC) $(OBJS) -lpthread -o $(NAME) -g

sanitize: fclean $(OBJS) #Linux
	@$(CC) $(OBJS) -lpthread -o $(NAME) -g -fsanitize=address

clean:
	@rm -f $(OBJS)

fclean: clean
	@rm -f $(NAME)

re: fclean all