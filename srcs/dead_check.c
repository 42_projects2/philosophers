/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dead_check.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/08 12:39:33 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/29 13:32:17 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	all_fed(t_uni *uni)
{
	int	i;

	i = -1;
	while (uni->eat_needs != -1 && ++i < uni->ph_count
		&& uni->philos[i]->eaten >= uni->eat_needs)
	{
		if (i == uni->ph_count - 1)
		{
			uni->stop = 1;
			return (1);
		}
	}
	return (0);
}

void	*dead_check(void *arg)
{
	t_uni	*uni;
	int		i;

	uni = (t_uni *)arg;
	while (1)
	{
		if (uni->stop == 1 || all_fed(uni) == 1)
			return (NULL);
		i = -1;
		while (++i < uni->ph_count && uni->stop == 0)
		{
			if (pthread_mutex_lock(&uni->print_lock) != 0)
				error_exit("ERROR\nmutex lock failed", uni);
			if (get_millis() - uni->start_millis - uni->philos[i]->last_meal
				>= uni->time_to_die)
			{
				uni->stop = 1;
				printf("%ld\t\t\e[1;92m%d\e[0m%s %s\e[0m\n",
					get_millis() - uni->start_millis, i + 1, "\e[0;31m", "died");
			}
			if (pthread_mutex_unlock(&uni->print_lock) != 0)
				error_exit("ERROR\nmutex unlock failed", uni);
		}
	}
	return (NULL);
}
