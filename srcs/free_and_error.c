/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_and_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/02 00:47:31 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/29 10:34:26 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	free_stuff(t_uni *uni)
{
	int	i;

	if (uni->thread)
		free(uni->thread);
	if (uni->f_locks)
		free(uni->f_locks);
	i = -1;
	if (uni->philos)
	{	
		while (++i < uni->ph_count)
			free(uni->philos[i]);
		free(uni->philos);
	}
	if (uni)
		free(uni);
}

void	error_exit(char *err_str, t_uni *uni)
{
	if (uni)
		free_stuff(uni);
	printf("%s\n", err_str);
	exit(EXIT_FAILURE);
}
