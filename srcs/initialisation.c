/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialisation.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/02 00:50:00 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/29 13:33:51 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

t_ph	**init_philos(t_uni *uni)
{
	t_ph	**philos;
	int		i;

	philos = ft_calloc(uni->ph_count, sizeof(*philos));
	if (!philos)
		error_exit("ERROR\nallocation error", uni);
	i = -1;
	while (++i < uni->ph_count)
	{
		philos[i] = ft_calloc(1, sizeof(t_ph));
		if (!philos[i])
		{
			while (--i >= 0)
				free(philos[i]);
			free(philos);
			error_exit("ERROR\nallocation error", uni);
		}
		philos[i]->uni = uni;
		philos[i]->num_of_philo = i;
		philos[i]->eaten = 0;
		philos[i]->last_meal = 0;
	}
	return (philos);
}

void	init_mutexes(t_uni *uni)
{
	int	i;

	uni->f_locks = ft_calloc(uni->ph_count,
			sizeof(pthread_mutex_t));
	i = -1;
	while (++i < uni->ph_count)
	{
		if (pthread_mutex_init(&uni->f_locks[i], NULL) != 0)
			error_exit("ERROR\nmutex initialisation failed", uni);
	}
	if (pthread_mutex_init(&uni->print_lock, NULL) != 0)
		error_exit("ERROR\nmutex initialisation failed", uni);
}

t_uni	*init_uni(int argc, char *argv[])
{
	t_uni	*uni;

	uni = ft_calloc(1, sizeof(t_uni));
	if (!uni)
		error_exit("ERROR\nallocation failed", NULL);
	uni->ph_count = philo_atoi(argv[1]);
	uni->time_to_die = philo_atoi(argv[2]);
	uni->time_to_eat = philo_atoi(argv[3]);
	uni->time_to_sleep = philo_atoi(argv[4]);
	uni->stop = 0;
	if (argc == 6)
		uni->eat_needs = philo_atoi(argv[5]);
	else
		uni->eat_needs = -1;
	uni->thread = ft_calloc(uni->ph_count + 1, sizeof(pthread_t));
	if (!(uni->thread))
		error_exit("ERROR\nallocation failed", uni);
	if (uni->ph_count < 0 || uni->time_to_die < 0
		|| uni->time_to_eat < 0 || uni->time_to_sleep < 0
		|| (argc == 6 && uni->eat_needs < 0))
		error_exit("ERROR\ninvalid argument", uni);
	uni->start_millis = get_millis();
	uni->philos = init_philos(uni);
	init_mutexes(uni);
	return (uni);
}
