/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/31 22:06:46 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/29 10:34:35 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	get_start_times(t_uni *uni)
{
	int		i;
	int		j;
	long	real_delay;

	if (uni->delay == 0)
		real_delay = 1;
	else
		real_delay = uni->delay;
	i = 0;
	j = 0;
	while (i < uni->ph_count)
	{
		uni->philos[i]->start_time = real_delay * j;
		i = i + 2;
		j++;
	}
	i = 1;
	while (i < uni->ph_count)
	{
		uni->philos[i]->start_time = real_delay * j;
		i = i + 2;
		j++;
	}
}

void	create_and_join_philos(t_uni *uni)
{
	int	i;

	i = -1;
	while (++i < uni->ph_count)
	{
		if (pthread_create(&uni->thread[i], NULL, th_f,
				(void *)uni->philos[i]) != 0)
		{
			uni->stop = 1;
			while (--i >= 0)
				pthread_join(uni->thread[i], NULL);
			pthread_join(uni->thread[uni->ph_count], NULL);
			error_exit("ERROR\nfailed to create a thread", uni);
		}
	}
	while (--i >= 0)
		pthread_join(uni->thread[i], NULL);
	pthread_join(uni->thread[uni->ph_count], NULL);
}

int	main(int argc, char *argv[])
{
	t_uni		*uni;

	if (argc != 5 && argc != 6)
		error_exit("ERROR\nwrong number of arguments", NULL);
	if (philo_atoi(argv[1]) == 0)
		return (0);
	uni = init_uni(argc, argv);
	uni->delay = 0;
	if (uni->ph_count != 1 && uni->ph_count % 2 != 0)
		uni->delay = (long)((uni->time_to_eat) / (uni->ph_count / 2));
	pthread_create(&uni->thread[uni->ph_count], NULL, dead_check, (void *)uni);
	if (uni->ph_count % 2 != 0)
		get_start_times(uni);
	create_and_join_philos(uni);
	free_stuff(uni);
	return (0);
}
