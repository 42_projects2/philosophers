/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/31 22:06:44 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/29 13:32:17 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/time.h>
# include <pthread.h>

typedef struct s_uni
{
	int				ph_count;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				eat_needs;
	int				stop;
	pthread_mutex_t	print_lock;
	pthread_t		*thread;
	pthread_mutex_t	*f_locks;
	struct s_ph		**philos;
	long			start_millis;
	long			delay;
}	t_uni;

typedef struct s_ph
{
	struct s_uni	*uni;
	int				num_of_philo;
	long			start_time;
	int				eaten;
	long			last_meal;
}	t_ph;

// main
int		main(int argc, char *argv[]);
// philo_threads
void	*th_f(void *arg);
// initialisation
t_uni	*init_uni(int argc, char *argv[]);
// dead_check
void	*dead_check(void *arg);
// utilities
int		philo_atoi(const char *str);
void	*ft_calloc(size_t count, size_t size);
long	get_millis(void);
void	print_state_change(t_ph *philo, char *str, char *color);
// free_and_error
void	free_stuff(t_uni *general);
void	error_exit(char *err_str, t_uni *general);

#endif