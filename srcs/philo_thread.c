/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_thread.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/28 13:05:59 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/29 13:20:13 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	unlock_forks(t_ph *ph)
{
	if (pthread_mutex_unlock(&ph->uni->f_locks[ph->num_of_philo]) != 0)
		error_exit("ERROR\nmutex unlock failed", ph->uni);
	if (ph->num_of_philo == 0)
	{
		if (pthread_mutex_unlock(&ph->uni->f_locks[ph->uni->ph_count - 1]) != 0)
			error_exit("ERROR\nmutex unlock failed", ph->uni);
	}
	else
	{
		if (pthread_mutex_unlock(&ph->uni->f_locks[ph->num_of_philo - 1]) != 0)
			error_exit("ERROR\nmutex unlock failed", ph->uni);
	}
}

int	lock_forks(t_ph *ph)
{
	if (ph->uni->stop == 1)
		return (1);
	if (pthread_mutex_lock(&ph->uni->f_locks[ph->num_of_philo]) != 0)
		error_exit("ERROR\nmutex lock failed", ph->uni);
	print_state_change(ph, "has taken a fork", "");
	if (ph->uni->stop == 1 || ph->uni->ph_count == 1)
	{
		if (pthread_mutex_unlock(&ph->uni->f_locks[ph->num_of_philo]) != 0)
			error_exit("ERROR\nmutex unlock failed", ph->uni);
		return (1);
	}
	if (ph->num_of_philo == 0)
	{
		if (pthread_mutex_lock(&ph->uni->f_locks[ph->uni->ph_count - 1]) != 0)
			error_exit("ERROR\nmutex lock failed", ph->uni);
		print_state_change(ph, "has taken a fork", "");
	}
	else
	{
		if (pthread_mutex_lock(&ph->uni->f_locks[ph->num_of_philo - 1]) != 0)
			error_exit("ERROR\nmutex lock failed", ph->uni);
		print_state_change(ph, "has taken a fork", "");
	}
	return (0);
}

void	wait_to_start(t_ph *ph)
{
	if (ph->uni->ph_count % 2 != 0 && ph->uni->delay == 0)
		usleep((1000 / ph->uni->ph_count) * ph->start_time);
	else if (ph->uni->ph_count % 2 != 0)
	{
		while (get_millis() - ph->uni->start_millis < ph->start_time)
			usleep(100);
	}
	if (ph->uni->ph_count % 2 == 0 && ph->num_of_philo % 2 != 0)
	{
		while (get_millis() - ph->uni->start_millis < ph->uni->time_to_eat)
			usleep(100);
	}
}

void	*th_f(void *arg)
{
	t_ph	*ph;

	ph = (t_ph *)arg;
	wait_to_start(ph);
	while (1)
	{
		if (lock_forks(ph) == 1)
			break ;
		print_state_change(ph, "is eating", "\e[0;35m");
		while (ph->uni->stop == 0 && get_millis() - ph->uni->start_millis
			< ph->last_meal + ph->uni->time_to_eat)
			usleep(100);
		unlock_forks(ph);
		ph->eaten++;
		print_state_change(ph, "is sleeping", "\e[0;34m");
		usleep((useconds_t)ph->uni->time_to_sleep * 1000);
		print_state_change(ph, "is thinking", "\e[0;33m");
	}
	return (NULL);
}
