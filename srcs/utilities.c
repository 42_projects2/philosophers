/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utilities.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/31 22:59:16 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/29 13:32:17 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static int	get_nbr(int *i, const char *str)
{
	int	nbr;

	nbr = 0;
	if (str[*i] >= '0' && str[*i] <= '9')
	{
		while (str[*i] != '\0')
		{
			if (str[*i] >= '0' && str[*i] <= '9')
				nbr = nbr * 10 + (str[(*i)++] - '0');
			else
				return (-1);
		}
	}
	else
		return (-1);
	return (nbr);
}

int	philo_atoi(const char *str)
{
	int	i;
	int	nbr;
	int	mins;

	i = 0;
	nbr = 0;
	mins = 0;
	while ((str[i] >= '\t' && str[i] <= '\r') || str[i] == ' ')
		i++;
	while (str[i] == '-')
	{
		mins++;
		i++;
	}
	if (str[i] == '+')
		i++;
	nbr = get_nbr(&i, str);
	if (nbr == -1)
		return (-1);
	if ((mins % 2) != 0)
		return (nbr * -1);
	return (nbr);
}

void	*ft_calloc(size_t count, size_t size)
{
	void	*rtp;
	size_t	full_size;

	full_size = count * size;
	rtp = malloc(full_size);
	if (rtp == NULL)
		return (NULL);
	while (full_size-- > 0)
		((char *)rtp)[full_size] = '\0';
	return (rtp);
}

long	get_millis(void)
{
	long			millis;
	struct timeval	time;

	if (gettimeofday(&time, NULL) == 0)
	{
		millis = (time.tv_sec * 1000) + (time.tv_usec / 1000);
		return (millis);
	}
	return (-1);
}

void	print_state_change(t_ph *philo, char *str, char *color)
{
	long	millis;

	if (pthread_mutex_lock(&philo->uni->print_lock) != 0)
		error_exit("ERROR\nmutex lock failed", philo->uni);
	millis = get_millis() - philo->uni->start_millis;
	if (str[3] == 'e')
		philo->last_meal = millis;
	if (philo->uni->stop == 0)
	{
		if (str[0] == 'd')
			philo->uni->stop = 1;
		printf("%ld\t\t\e[1;92m%d\e[0m%s %s\e[0m\n", millis,
			philo->num_of_philo + 1, color, str);
	}
	if (pthread_mutex_unlock(&philo->uni->print_lock) != 0)
		error_exit("ERROR\nmutex unlock failed", philo->uni);
}
